using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    Rigidbody rb;
    Rigidbody2D rb2D;
    

    public float transformMovementSpeed = 30.0f;
    public float rigidbodyMovementForce = 500;

    public bool rigid2d = false;
    public bool rigid = false;

	void Start ()
    {
        rb = this.GetComponent<Rigidbody>();
        rb2D = this.GetComponent<Rigidbody2D>();
       

        Debug.Log("Utiliza las flechas para desplazarte");
    }

	void Update ()
    {
        if (!hasAuthority) return;

        

        if (rigid == true)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Debug.Log("abajo");

                rb.AddForce(new Vector3(0, -1.5f, -1) * rigidbodyMovementForce);
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                rb.AddForce(new Vector3(0, 1.5f, 1) * rigidbodyMovementForce);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                rb.AddForce(new Vector3(-1, 0, 0) * rigidbodyMovementForce);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                rb.AddForce(new Vector3(1, 0, 0) * rigidbodyMovementForce);
            }
        }
        else if (rigid2d == true)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                rb2D.AddForce(new Vector3(0, -1.5f, -1) * rigidbodyMovementForce);
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                rb2D.AddForce(new Vector3(0, 1.5f, 1) * rigidbodyMovementForce);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                rb2D.AddForce(new Vector3(-1, 0, 0) * rigidbodyMovementForce);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                rb2D.AddForce(new Vector3(1, 0, 0) * rigidbodyMovementForce);
            }
        }
        else
        {
            float speed = transformMovementSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.DownArrow))
            {
                transform.position = transform.position + new Vector3(0, -1.5f, -1) * speed;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                transform.position = transform.position + new Vector3(0, 1.5f, 1) * speed;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.position = transform.position + new Vector3(-1, 0, 0) * speed;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.position = transform.position + new Vector3(1, 0, 0) * speed;
            }
        }

    }

}
