﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.Serialization;


public enum SyncMode
{
    XYZ, XY, XZ, YZ, X, Y, Z, NONE
}

public class SyncMovement : NetworkBehaviour
{


    #region Configuration

     short FromServerToProxy = 30001;
     short FromOwnerToServer = 30002;

     float interpolationBackTime = .1f;
     float extrapolationTimeLimit = .3f;
     float extrapolationDistanceLimit = .3f;
     float sendPositionLimit = .001f;
     float sendRotationLimit = .001f;
     float sendScaleLimit = .001f;
     float sendVelocityLimit = .001f;
     float sendAngularVelocityLimit = .001f;
     float receivedPositionLimit = 0.0f;
     float receivedRotationLimit = 0.0f;
     float positionAutoLimit = 8;
     float rotationAutoLimit = 60;
     float scaleAutoLimit = 3;

    [Range(0, 1)]
     float positionLerpSpeed = .2f;

    [Range(0, 1)]
     float rotationLerpSpeed = .2f;

    [Range(0, 1)]
     float scaleLerpSpeed = .2f;

     SyncMode syncPosition = SyncMode.XYZ;
     SyncMode syncRotation = SyncMode.XYZ;
     SyncMode syncScale = SyncMode.XYZ;
     SyncMode syncVelocity = SyncMode.XYZ;
     SyncMode syncAngularVelocity = SyncMode.XYZ;


    public enum WhereToUpdate
    {
        Update, FixedUpdate
    }
    public WhereToUpdate whereToUpdateTFM = WhereToUpdate.FixedUpdate;

    public float sendRate = 30;

    public int networkChannel = Channels.DefaultUnreliable;

    #endregion

    #region Variables

    [NonSerialized]
    public State[] stateBuffer;              

    [NonSerialized]
    public int stateCount;

    [NonSerialized]
    public Rigidbody rigid;
    [NonSerialized]
    public bool hasRigdibody = false;
    [NonSerialized]
    public Rigidbody2D rigid2D;
    [NonSerialized]
    public bool hasRigidbody2D = false;

    bool skipLerp = false;
    bool dontLerp = false;


    [NonSerialized]
    public float lastTeleportOwnerTime;

    [NonSerialized]
    public float lastTimeStateWasSent;

    [NonSerialized]
    public float lastTimeStateWasReceived;

    [NonSerialized]
    public Vector3 lastPosition;

    [NonSerialized]
    public Quaternion lastRotation = Quaternion.identity;

    [NonSerialized]
    public Vector3 lastScale;

    [NonSerialized]
    public Vector3 lastVelocity;

    [NonSerialized]
    public Vector3 lastAngularVelocity;

    NetworkIdentity netIdentifier;

    [NonSerialized]
    public GameObject GameObjectToSync;


    [NonSerialized]
    public int syncIndex = 0;

    State extrapolationEndState;
    float extrapolationStopTime;

    [NonSerialized]
    public bool forceSendState = false;

    [NonSerialized]
    public bool sendPosition;
    [NonSerialized]
    public bool sendRotation;
    [NonSerialized]
    public bool sendScale;
    [NonSerialized]
    public bool sendVelocity;
    [NonSerialized]
    public bool sendAngularVelocity;

    #endregion

    void Start () {
		
	}

    void Awake()
    {
        int calculatedStateBufferSize = ((int)(sendRate * interpolationBackTime) + 1) * 2;
        stateBuffer = new State[Mathf.Max(calculatedStateBufferSize, 30)];

        netIdentifier = this.GetComponent<NetworkIdentity>();
        rigid = this.GetComponent<Rigidbody>();
        rigid2D = this.GetComponent<Rigidbody2D>();

        if (rigid != null)
        {
            hasRigdibody = true;
            Debug.Log("hasRigdibody=" + hasRigdibody);
        }

        if (rigid2D != null)
        {
            hasRigidbody2D = true;
            if (syncVelocity != SyncMode.NONE) syncVelocity = SyncMode.XY;
            if (syncAngularVelocity != SyncMode.NONE) syncAngularVelocity = SyncMode.Z;
            Debug.Log("hasRigdibody2d=" + hasRigidbody2D);

        }

        if (!rigid && !rigid2D)
        {
            syncVelocity = SyncMode.NONE;
            syncAngularVelocity = SyncMode.NONE;
        }

            GameObjectToSync = this.gameObject;

    }

    void Update()
    {
        if (!hasAuthority && whereToUpdateTFM == WhereToUpdate.Update)
        {
            applyInterAndExtra();
        }

        if (!hasAuthority || (!NetworkServer.active && !ClientScene.ready)) return;

        if (Time.realtimeSinceStartup - lastTimeStateWasSent <  1f / sendRate 
            && !forceSendState) return;

        sendPosition = shouldSendPosition();
        sendRotation = shouldSendRotation();
        sendScale = shouldSendScale();
        sendVelocity = shouldSendVelocity();
        sendAngularVelocity = shouldSendAngularVelocity();

        if (!sendPosition && !sendRotation && !sendScale && !sendVelocity && !sendAngularVelocity) return;

        lastTimeStateWasSent = Time.realtimeSinceStartup;

        NetworkState networkState = new NetworkState(this);

        if (NetworkServer.active)
        {
            SendStateToProxy(networkState);

            if (sendPosition) lastPosition = networkState.state.position;
            if (sendRotation) lastRotation = networkState.state.rotation;
            if (sendScale) lastScale = networkState.state.scale;
            if (sendVelocity) lastVelocity = networkState.state.velocity;
            if (sendAngularVelocity) lastAngularVelocity = networkState.state.angularVelocity;
        }
        else
        {
            NetworkManager.singleton.client.connection.SendByChannel(FromOwnerToServer, networkState, networkChannel);
        }

        forceSendState = false;
    }

    void FixedUpdate()
    {
        if (!hasAuthority && whereToUpdateTFM == WhereToUpdate.FixedUpdate)
        {
            applyInterAndExtra();
        }
    }


    #region UNET

    public override void OnStartServer()
    {
        if (GetComponent<NetworkIdentity>().localPlayerAuthority)
        {
            NetworkServer.RegisterHandler(FromOwnerToServer, HandleFromOwnerToServer);
            if (NetworkManager.singleton.client != null)
            {
                NetworkManager.singleton.client.RegisterHandler(FromServerToProxy, HandleFromServerToProxy);
            }
        }
        else
        {

            Debug.LogError("Activa localPlayerAuthority en el Player Prefab");
            return;
        }
    }

    public override void OnStartClient()
    {
        if (!NetworkServer.active)
        {
            NetworkManager.singleton.client.RegisterHandler(FromServerToProxy, HandleFromServerToProxy);
        }
    }

    #endregion



    #region Internal

    void applyInterAndExtra()
    {
        if (stateCount == 0) return;

        State targetState;
        bool triedToExtrapolateTooFar = false;

        if (dontLerp)
        {
            targetState = new State(this);
        }
        else
        {
            float interpolationTime = approximateNetworkTimeOnOwner - interpolationBackTime * 1000;

            if (stateCount > 1 && stateBuffer[0].ownerTimestamp > interpolationTime)
            {
                interpolate(interpolationTime, out targetState);
            }
            else
            {
                bool success = extrapolate(interpolationTime, out targetState);
                triedToExtrapolateTooFar = !success;
            }
        }

        float actualPositionLerpSpeed = positionLerpSpeed;
        float actualRotationLerpSpeed = rotationLerpSpeed;
        float actualScaleLerpSpeed = scaleLerpSpeed;

        if (skipLerp)
        {
            actualPositionLerpSpeed = 1;
            actualRotationLerpSpeed = 1;
            actualScaleLerpSpeed = 1;
            skipLerp = false;
            dontLerp = false;
        }
        else if (dontLerp)
        {
            actualPositionLerpSpeed = 1;
            actualRotationLerpSpeed = 1;
            actualScaleLerpSpeed = 1;
            stateCount = 0;
        }

        if (!triedToExtrapolateTooFar || (!hasRigdibody && !hasRigidbody2D))
        {
            bool changedPositionEnough = false;
            float distance = 0;
            if (getPosition() != targetState.position)
            {
                if (positionAutoLimit != 0 || receivedPositionLimit != 0)
                {
                    distance = Vector3.Distance(getPosition(), targetState.position);
                }
            }
            if (receivedPositionLimit != 0)
            {
                if (distance > receivedPositionLimit)
                {
                    changedPositionEnough = true;
                }
            }
            else               
            {
                changedPositionEnough = true;
            }

            bool changedRotationEnough = false;
            float angleDifference = 0;
            if (getRotation() != targetState.rotation)
            {
                if (rotationAutoLimit != 0 || receivedRotationLimit != 0)
                {
                    angleDifference = Quaternion.Angle(getRotation(), targetState.rotation);
                }
            }
            if (receivedRotationLimit != 0)
            {
                if (angleDifference > receivedRotationLimit)
                {
                    changedRotationEnough = true;
                }
            }
            else               
            {
                changedRotationEnough = true;
            }

            bool changedScaleEnough = false;
            float scaleDistance = 0;
            if (getScale() != targetState.scale)
            {
                changedScaleEnough = true;
                if (scaleAutoLimit != 0)
                {
                    scaleDistance = Vector3.Distance(getScale(), targetState.scale);
                }
            }

            if (hasRigdibody && !rigid.isKinematic)
            {
                if (changedPositionEnough)
                {
                    Vector3 newVelocity = rigid.velocity;
                    if (isSyncingXVelocity)
                    {
                        newVelocity.x = targetState.velocity.x;
                    }
                    if (isSyncingYVelocity)
                    {
                        newVelocity.y = targetState.velocity.y;
                    }
                    if (isSyncingZVelocity)
                    {
                        newVelocity.z = targetState.velocity.z;
                    }
                    rigid.velocity = Vector3.Lerp(rigid.velocity, newVelocity, actualPositionLerpSpeed);
                }
                else
                {
                    rigid.velocity = Vector3.zero;
                    rigid.angularVelocity = Vector3.zero;
                }
                if (changedRotationEnough)
                {
                    Vector3 newAngularVelocity = rigid.angularVelocity;
                    if (isSyncingXAngularVelocity)
                    {
                        newAngularVelocity.x = targetState.angularVelocity.x;
                    }
                    if (isSyncingYAngularVelocity)
                    {
                        newAngularVelocity.y = targetState.angularVelocity.y;
                    }
                    if (isSyncingZAngularVelocity)
                    {
                        newAngularVelocity.z = targetState.angularVelocity.z;
                    }
                    rigid.angularVelocity = Vector3.Lerp(rigid.angularVelocity, newAngularVelocity, actualRotationLerpSpeed);
                }
                else
                {
                    rigid.angularVelocity = Vector3.zero;
                }
            }
            else if (hasRigidbody2D && !rigid2D.isKinematic)
            {
                if (syncVelocity == SyncMode.XY)
                {
                    if (changedPositionEnough)
                    {
                        rigid2D.velocity = Vector2.Lerp(rigid2D.velocity, targetState.velocity, actualPositionLerpSpeed);
                    }
                    else
                    {
                        rigid2D.velocity = Vector2.zero;
                    }
                }
                if (syncAngularVelocity == SyncMode.Z)
                {
                    if (changedRotationEnough)
                    {
                        rigid2D.angularVelocity = Mathf.Lerp(rigid2D.angularVelocity, targetState.angularVelocity.z, actualRotationLerpSpeed);
                    }
                    else
                    {
                        rigid2D.angularVelocity = 0;
                    }
                }
            }


            if (syncPosition != SyncMode.NONE)
            {
                if (changedPositionEnough)
                {
                    bool shouldTeleport = false;
                    if (distance > positionAutoLimit)
                    {
                        actualPositionLerpSpeed = 1;
                        shouldTeleport = true;
                    }
                    Vector3 newPosition = getPosition();
                    if (isSyncingXPosition)
                    {
                        newPosition.x = targetState.position.x;
                    }
                    if (isSyncingYPosition)
                    {
                        newPosition.y = targetState.position.y;
                    }
                    if (isSyncingZPosition)
                    {
                        newPosition.z = targetState.position.z;
                    }
                    setPosition(Vector3.Lerp(getPosition(), newPosition, actualPositionLerpSpeed), shouldTeleport);
                }
            }

            if (syncRotation != SyncMode.NONE)
            {
                if (changedRotationEnough)
                {
                    bool shouldTeleport = false;
                    if (angleDifference > rotationAutoLimit)
                    {
                        actualRotationLerpSpeed = 1;
                        shouldTeleport = true;
                    }
                    Vector3 newRotation = getRotation().eulerAngles;
                    if (isSyncingXRotation)
                    {
                        newRotation.x = targetState.rotation.eulerAngles.x;
                    }
                    if (isSyncingYRotation)
                    {
                        newRotation.y = targetState.rotation.eulerAngles.y;
                    }
                    if (isSyncingZRotation)
                    {
                        newRotation.z = targetState.rotation.eulerAngles.z;
                    }
                    Quaternion newQuaternion = Quaternion.Euler(newRotation);
                    setRotation(Quaternion.Lerp(getRotation(), newQuaternion, actualRotationLerpSpeed), shouldTeleport);
                }
            }

            if (syncScale != SyncMode.NONE)
            {
                if (changedScaleEnough)
                {
                    bool shouldTeleport = false;
                    if (scaleDistance > scaleAutoLimit)
                    {
                        actualScaleLerpSpeed = 1;
                        shouldTeleport = true;
                    }
                    Vector3 newScale = getScale();
                    if (isSyncingXScale)
                    {
                        newScale.x = targetState.scale.x;
                    }
                    if (isSyncingYScale)
                    {
                        newScale.y = targetState.scale.y;
                    }
                    if (isSyncingZScale)
                    {
                        newScale.z = targetState.scale.z;
                    }
                    setScale(Vector3.Lerp(getScale(), newScale, actualScaleLerpSpeed), shouldTeleport);
                }
            }
        }
        else if (triedToExtrapolateTooFar)
        {
            if (hasRigdibody)
            {
                rigid.velocity = Vector3.zero;
                rigid.angularVelocity = Vector3.zero;
            }
            if (hasRigidbody2D)
            {
                rigid2D.velocity = Vector2.zero;
                rigid2D.angularVelocity = 0;
            }
        }
    }

    void interpolate(float interpolationTime, out State targetState)
    {
        int stateIndex = 0;
        for (; stateIndex < stateCount; stateIndex++)
        {
            if (stateBuffer[stateIndex].ownerTimestamp <= interpolationTime) break;
        }

        if (stateIndex == stateCount)
        {
            stateIndex--;
        }

        State end = stateBuffer[Mathf.Max(stateIndex - 1, 0)];
        State start = stateBuffer[stateIndex];

        float t = (interpolationTime - start.ownerTimestamp) / (end.ownerTimestamp - start.ownerTimestamp);

        targetState = State.Lerp(start, end, t);
    }

    bool extrapolate(float interpolationTime, out State targetState)                      
    {
        targetState = new State(stateBuffer[0]);

        float extrapolationLength = (interpolationTime - targetState.ownerTimestamp) / 1000.0f;

        if (syncVelocity == SyncMode.NONE || targetState.velocity.magnitude < sendVelocityLimit)
        {
            return true;
        }

        if (((hasRigdibody && !rigid.isKinematic) || (hasRigidbody2D && !rigid2D.isKinematic)))
        {
            float simulatedTime = 0;
            while (simulatedTime < extrapolationLength)
            {
                if (simulatedTime > extrapolationTimeLimit)
                {
                    if (extrapolationStopTime < lastTimeStateWasReceived)
                    {
                        extrapolationEndState = targetState;
                    }
                    extrapolationStopTime = Time.realtimeSinceStartup;
                    targetState = extrapolationEndState;
                    return false;
                }

                float timeDif = Mathf.Min(Time.fixedDeltaTime, extrapolationLength - simulatedTime);

                targetState.position += targetState.velocity * timeDif;

                if (hasRigdibody && rigid.useGravity)
                {
                    targetState.velocity += Physics.gravity * timeDif;
                }
                else if (hasRigidbody2D)
                {
                    targetState.velocity += Physics.gravity * rigid2D.gravityScale * timeDif;
                }

                if (hasRigdibody)
                {
                    targetState.velocity -= targetState.velocity * timeDif * rigid.drag;
                }
                else if (hasRigidbody2D)
                {
                    targetState.velocity -= targetState.velocity * timeDif * rigid2D.drag;
                }

                float axisLength = timeDif * targetState.angularVelocity.magnitude * Mathf.Rad2Deg;
                Quaternion angularRotation = Quaternion.AngleAxis(axisLength, targetState.angularVelocity);
                targetState.rotation = angularRotation * targetState.rotation;

                if (Vector3.Distance(stateBuffer[0].position, targetState.position) >= extrapolationDistanceLimit)
                {
                    extrapolationEndState = targetState;
                    extrapolationStopTime = Time.realtimeSinceStartup;
                    targetState = extrapolationEndState;
                    return false;
                }

                simulatedTime += Time.fixedDeltaTime;
            }
        }

        return true;
    }

    public Vector3 getPosition()
    {
            return GameObjectToSync.transform.position;
    }


    public Quaternion getRotation()
    {
            return GameObjectToSync.transform.rotation;
    }


    public Vector3 getScale()
    {
        return GameObjectToSync.transform.localScale;
    }


    public void setPosition(Vector3 position, bool isTeleporting)
    {
            if (hasRigdibody && !isTeleporting && whereToUpdateTFM != WhereToUpdate.Update)
            {
                rigid.MovePosition(position);
            }
            if (hasRigidbody2D && !isTeleporting && whereToUpdateTFM != WhereToUpdate.Update)
            {
                rigid2D.MovePosition(position);
            }
            else
            {
                GameObjectToSync.transform.position = position;
            }
    }


    public void setRotation(Quaternion rotation, bool isTeleporting)
    {
            if (hasRigdibody && !isTeleporting && whereToUpdateTFM != WhereToUpdate.Update)
            {
                rigid.MoveRotation(rotation);
            }
            if (hasRigidbody2D && !isTeleporting && whereToUpdateTFM != WhereToUpdate.Update)
            {
                rigid2D.MoveRotation(rotation.eulerAngles.z);
            }
            else
            {
                GameObjectToSync.transform.rotation = rotation;
            }
    }


    public void setScale(Vector3 scale, bool isTeleporting)
    {
        GameObjectToSync.transform.localScale = scale;
    }

    #endregion

    #region States

    public void addState(State state)
    {
        if (stateCount > 1 && state.ownerTimestamp < stateBuffer[0].ownerTimestamp)
        {
            Debug.LogError("Received state out of order for: " + GameObjectToSync.name);
            return;
        }

        lastTimeStateWasReceived = Time.realtimeSinceStartup;

        for (int i = stateBuffer.Length - 1; i >= 1; i--)
        {
            stateBuffer[i] = stateBuffer[i - 1];
        }

        stateBuffer[0] = state;

        stateCount = Mathf.Min(stateCount + 1, stateBuffer.Length);
    }

    public void stopLerping()
    {
        dontLerp = true;
    }

    public void restartLerping()
    {
        if (!dontLerp) return;

        skipLerp = true;
    }


    public void clearBuffer()
    {
        stateCount = 0;
    }


    public void teleport(int networkTimestamp, Vector3 position, Quaternion rotation)
    {
        lastTeleportOwnerTime = networkTimestamp;
        setPosition(position, true);
        setRotation(rotation, true);
        clearBuffer();
        stopLerping();
    }


  

    #endregion

    #region Comprobation

    public bool shouldSendPosition()
    {
        if (forceSendState ||
            (getPosition() != lastPosition &&
            (sendPositionLimit == 0 || Vector3.Distance(lastPosition, getPosition()) > sendPositionLimit)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool shouldSendRotation()
    {
        if (forceSendState ||
            (getRotation() != lastRotation &&
            (sendRotationLimit == 0 || Quaternion.Angle(lastRotation, getRotation()) > sendRotationLimit)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool shouldSendScale()
    {
        if (forceSendState ||
            (getScale() != lastScale &&
            (sendScaleLimit == 0 || Vector3.Distance(lastScale, getScale()) > sendScaleLimit)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool shouldSendVelocity()
    {
        if (hasRigdibody)
        {
            if (forceSendState ||
                (rigid.velocity != lastVelocity &&
                (sendVelocityLimit == 0 || Vector3.Distance(lastVelocity, rigid.velocity) > sendVelocityLimit)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (hasRigidbody2D)
        {
            if (forceSendState ||
                ((rigid2D.velocity.x != lastVelocity.x && rigid2D.velocity.y != lastVelocity.y) &&
                (sendVelocityLimit == 0 || Vector2.Distance(lastVelocity, rigid2D.velocity) > sendVelocityLimit)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public bool shouldSendAngularVelocity()
    {
        if (hasRigdibody)
        {
            if (forceSendState ||
                (rigid.angularVelocity != lastAngularVelocity &&
                (sendAngularVelocityLimit == 0 ||
                Vector3.Distance(lastAngularVelocity, rigid.angularVelocity) > sendAngularVelocityLimit)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (hasRigidbody2D)
        {
            if (forceSendState ||
                (rigid2D.angularVelocity != lastAngularVelocity.z &&
                (sendAngularVelocityLimit == 0 ||
                Mathf.Abs(lastAngularVelocity.z - rigid2D.angularVelocity) > sendAngularVelocityLimit)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

  

    public bool isSyncingXPosition
    {
        get
        {
            return syncPosition == SyncMode.XYZ         || syncPosition == SyncMode.XY || syncPosition == SyncMode.XZ || syncPosition == SyncMode.X;
        }
    }
    public bool isSyncingYPosition
    {
        get
        {
            return syncPosition == SyncMode.XYZ         || syncPosition == SyncMode.XY ||  syncPosition == SyncMode.YZ || syncPosition == SyncMode.Y;
        }
    }
    public bool isSyncingZPosition
    {
        get
        {
            return syncPosition == SyncMode.XYZ         ||  syncPosition == SyncMode.XZ ||   syncPosition == SyncMode.YZ ||  syncPosition == SyncMode.Z;
        }
    }
    public bool isSyncingXRotation
    {
        get
        {
            return syncRotation == SyncMode.XYZ         ||  syncRotation == SyncMode.XY || syncRotation == SyncMode.XZ ||   syncRotation == SyncMode.X;
        }
    }
    public bool isSyncingYRotation
    {
        get
        {
            return syncRotation == SyncMode.XYZ         ||  syncRotation == SyncMode.XY ||   syncRotation == SyncMode.YZ ||  syncRotation == SyncMode.Y;
        }
    }
    public bool isSyncingZRotation
    {
        get
        {
            return syncRotation == SyncMode.XYZ         ||  syncRotation == SyncMode.XZ ||   syncRotation == SyncMode.YZ ||  syncRotation == SyncMode.Z;
        }
    }
    public bool isSyncingXScale
    {
        get
        {
            return syncScale == SyncMode.XYZ            ||  syncScale == SyncMode.XY    ||   syncScale == SyncMode.XZ ||  syncScale == SyncMode.X;
        }
    }
    public bool isSyncingYScale
    {
        get
        {
            return syncScale == SyncMode.XYZ            ||   syncScale == SyncMode.XY   || syncScale == SyncMode.YZ || syncScale == SyncMode.Y;
        }
    }
    public bool isSyncingZScale
    {
        get
        {
            return syncScale == SyncMode.XYZ            || syncScale == SyncMode.XZ     || syncScale == SyncMode.YZ ||  syncScale == SyncMode.Z;
        }
    }
    public bool isSyncingXVelocity
    {
        get
        {
            return syncVelocity == SyncMode.XYZ         ||  syncVelocity == SyncMode.XY || syncVelocity == SyncMode.XZ ||   syncVelocity == SyncMode.X;
        }
    }
    public bool isSyncingYVelocity
    {
        get
        {
            return syncVelocity == SyncMode.XYZ         || syncVelocity == SyncMode.XY ||  syncVelocity == SyncMode.YZ || syncVelocity == SyncMode.Y;
        }
    }
    public bool isSyncingZVelocity
    {
        get
        {
            return syncVelocity == SyncMode.XYZ         || syncVelocity == SyncMode.XZ   || syncVelocity == SyncMode.YZ ||  syncVelocity == SyncMode.Z;
        }
    }
    public bool isSyncingXAngularVelocity
    {
        get
        {
            return syncAngularVelocity == SyncMode.XYZ  || syncAngularVelocity == SyncMode.XY   ||  syncAngularVelocity == SyncMode.XZ ||  syncAngularVelocity == SyncMode.X;
        }
    }
    public bool isSyncingYAngularVelocity
    {
        get
        {
            return syncAngularVelocity == SyncMode.XYZ  || syncAngularVelocity == SyncMode.XY   ||  syncAngularVelocity == SyncMode.YZ ||  syncAngularVelocity == SyncMode.Y;
        }
    }
    public bool isSyncingZAngularVelocity
    {
        get
        {
            return syncAngularVelocity == SyncMode.XYZ  || syncAngularVelocity == SyncMode.XZ   ||  syncAngularVelocity == SyncMode.YZ  ||  syncAngularVelocity == SyncMode.Z;
        }
    }




    #endregion


    #region Base NEtworking

    [Server]
    void SendStateToProxy(MessageBase state)
    {
        for (int i = 0; i < NetworkServer.connections.Count; i++)
        {
            NetworkConnection conn = NetworkServer.connections[i];

            if (conn != null && conn != netIdentifier.clientAuthorityOwner && conn.hostId != -1 && conn.isReady)
            {
                if (isObservedByConnection(conn) == false) continue;
                conn.SendByChannel(FromServerToProxy, state, networkChannel);
            }
        }
    }

    bool isObservedByConnection(NetworkConnection conn)
    {
        for (int i = 0; i < netIdentifier.observers.Count; i++)
        {
            if (netIdentifier.observers[i] == conn)
            {
                return true;
            }
        }
        return false;
    }

    static void HandleFromServerToProxy(NetworkMessage msg)
    {
        NetworkState networkState = msg.ReadMessage<NetworkState>();

        if (networkState != null && networkState.sync != null && !networkState.sync.hasAuthority)
        {
            networkState.sync.adjustOwnerTime(networkState.state.ownerTimestamp);
            if (networkState.state.ownerTimestamp > networkState.sync.lastTeleportOwnerTime)
            {
                networkState.sync.restartLerping();
                networkState.sync.addState(networkState.state);
            }
        }
    }

    static void HandleFromOwnerToServer(NetworkMessage msg)
    {
        NetworkState networkState = msg.ReadMessage<NetworkState>();

        networkState.sync.adjustOwnerTime(networkState.state.ownerTimestamp);
        networkState.sync.SendStateToProxy(networkState);
        if (networkState.state.ownerTimestamp > networkState.sync.lastTeleportOwnerTime)
        {
            networkState.sync.restartLerping();
            networkState.sync.addState(networkState.state);
        }
    }

    public override int GetNetworkChannel()
    {
        return networkChannel;
    }

    int _ownerTime;

    float lastTimeOwnerTimeWasSet;

    public int approximateNetworkTimeOnOwner
    {
        get
        {
            return _ownerTime + (int)((Time.realtimeSinceStartup - lastTimeOwnerTimeWasSet) * 1000);
        }
        set
        {
            _ownerTime = value;
            lastTimeOwnerTimeWasSet = Time.realtimeSinceStartup;
        }
    }

    void adjustOwnerTime(int ownerTimestamp)           
    {
        int newTime = ownerTimestamp;

        int maxTimeChange = 50;
        int timeChangeMagnitude = Mathf.Abs(approximateNetworkTimeOnOwner - newTime);
        if (approximateNetworkTimeOnOwner == 0 || timeChangeMagnitude < maxTimeChange || timeChangeMagnitude > maxTimeChange * 10)
        {
            approximateNetworkTimeOnOwner = newTime;
        }
        else
        {
            if (approximateNetworkTimeOnOwner < newTime)
            {
                approximateNetworkTimeOnOwner += maxTimeChange;
            }
            else
            {
                approximateNetworkTimeOnOwner -= maxTimeChange;
            }
        }
    }

    

    #endregion

}


public class State
{
    public int ownerTimestamp;
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
    public Vector3 velocity;
    public Vector3 angularVelocity;

    public bool serverShouldRelayPosition = false;
    public bool serverShouldRelayRotation = false;
    public bool serverShouldRelayScale = false;
    public bool serverShouldRelayVelocity = false;
    public bool serverShouldRelayAngularVelocity = false;

    public State() { }

    public State(State state)
    {
        ownerTimestamp = state.ownerTimestamp;
        position = state.position;
        rotation = state.rotation;
        scale = state.scale;
        velocity = state.velocity;
        angularVelocity = state.angularVelocity;
    }

    public State(SyncMovement syncscript)
    {
        ownerTimestamp = NetworkTransport.GetNetworkTimestamp();
        position = syncscript.getPosition();
        rotation = syncscript.getRotation();
        scale = syncscript.getScale();

        if (syncscript.hasRigdibody)
        {
            velocity = syncscript.rigid.velocity;
            angularVelocity = syncscript.rigid.angularVelocity;
        }
        else if (syncscript.hasRigidbody2D)
        {
            velocity = syncscript.rigid2D.velocity;
            angularVelocity = new Vector3(0, 0, syncscript.rigid2D.angularVelocity);
        }
        else
        {
            velocity = Vector3.zero;
            angularVelocity = Vector3.zero;
        }
    }

    public static State Lerp(State start, State end, float t)
    {
        State state = new State();

        state.position = Vector3.Lerp(start.position, end.position, t);
        state.rotation = Quaternion.Lerp(start.rotation, end.rotation, t);
        state.scale = Vector3.Lerp(start.scale, end.scale, t);
        state.velocity = Vector3.Lerp(start.velocity, end.velocity, t);
        state.angularVelocity = Vector3.Lerp(start.angularVelocity, end.angularVelocity, t);

        state.ownerTimestamp = (int)Mathf.Lerp(start.ownerTimestamp, end.ownerTimestamp, t);

        return state;
    }
}

public class NetworkState : MessageBase
{
    public SyncMovement sync;
    public State state = new State();

    byte positionMask = 1;         
    byte rotationMask = 2;         
    byte scaleMask = 4;         
    byte velocityMask = 8;         
    byte angularVelocityMask = 16;


    public NetworkState() { }

    public NetworkState(SyncMovement smooth)
    {
        this.sync = smooth;
        state = new State(smooth);
    }
    override public void Serialize(NetworkWriter writer)
    {
        bool sendPosition, sendRotation, sendScale, sendVelocity, sendAngularVelocity;

        if (NetworkServer.active && !sync.hasAuthority)
        {
            sendPosition = state.serverShouldRelayPosition;
            sendRotation = state.serverShouldRelayRotation;
            sendScale = state.serverShouldRelayScale;
            sendVelocity = state.serverShouldRelayVelocity;
            sendAngularVelocity = state.serverShouldRelayAngularVelocity;
        }
        else                
        {
            sendPosition = sync.sendPosition;
            sendRotation = sync.sendRotation;
            sendScale = sync.sendScale;
            sendVelocity = sync.sendVelocity;
            sendAngularVelocity = sync.sendAngularVelocity;
        }
        if (!NetworkServer.active)
        {
            if (sendPosition) sync.lastPosition = state.position;
            if (sendRotation) sync.lastRotation = state.rotation;
            if (sendScale) sync.lastScale = state.scale;
            if (sendVelocity) sync.lastVelocity = state.velocity;
            if (sendAngularVelocity) sync.lastAngularVelocity = state.angularVelocity;
        }

        writer.Write(encodeSyncInformation(sendPosition, sendRotation, sendScale, sendVelocity, sendAngularVelocity));
        writer.Write(sync.netId);
        writer.WritePackedUInt32((uint)sync.syncIndex);
        writer.WritePackedUInt32((uint)state.ownerTimestamp);

        if (sendPosition)
        {
                if (sync.isSyncingXPosition)
                {
                    writer.Write(state.position.x);
                }
                if (sync.isSyncingYPosition)
                {
                    writer.Write(state.position.y);
                }
                if (sync.isSyncingZPosition)
                {
                    writer.Write(state.position.z);
                }
        }
        if (sendRotation)
        {
            Vector3 rot = state.rotation.eulerAngles;
                if (sync.isSyncingXRotation)
                {
                    writer.Write(rot.x);
                }
                if (sync.isSyncingYRotation)
                {
                    writer.Write(rot.y);
                }
                if (sync.isSyncingZRotation)
                {
                    writer.Write(rot.z);
                }
        }
        if (sendScale)
        {
                if (sync.isSyncingXScale)
                {
                    writer.Write(state.scale.x);
                }
                if (sync.isSyncingYScale)
                {
                    writer.Write(state.scale.y);
                }
                if (sync.isSyncingZScale)
                {
                    writer.Write(state.scale.z);
                }
        }
        if (sendVelocity)
        {
                if (sync.isSyncingXVelocity)
                {
                    writer.Write(state.velocity.x);
                }
                if (sync.isSyncingYVelocity)
                {
                    writer.Write(state.velocity.y);
                }
                if (sync.isSyncingZVelocity)
                {
                    writer.Write(state.velocity.z);
                }
        }
        if (sendAngularVelocity)
        {
                if (sync.isSyncingXAngularVelocity)
                {
                    writer.Write(state.angularVelocity.x);
                }
                if (sync.isSyncingYAngularVelocity)
                {
                    writer.Write(state.angularVelocity.y);
                }
                if (sync.isSyncingZAngularVelocity)
                {
                    writer.Write(state.angularVelocity.z);
                }
        }
    }

    override public void Deserialize(NetworkReader reader)
    {
        byte syncInfoByte = reader.ReadByte();
        bool syncPosition = shouldSyncPosition(syncInfoByte);
        bool syncRotation = shouldSyncRotation(syncInfoByte);
        bool syncScale = shouldSyncScale(syncInfoByte);
        bool syncVelocity = shouldSyncVelocity(syncInfoByte);
        bool syncAngularVelocity = shouldSyncAngularVelocity(syncInfoByte);

        NetworkInstanceId netID = reader.ReadNetworkId();
        int syncIndex = (int)reader.ReadPackedUInt32();
        state.ownerTimestamp = (int)reader.ReadPackedUInt32();

        GameObject ob = null;
        if (NetworkServer.active)
        {
            ob = NetworkServer.FindLocalObject(netID);
        }
        else
        {
            ob = ClientScene.FindLocalObject(netID);
        }

        if (!ob)
        {
            Debug.LogError("Could not find target for network state message.");
            return;
        }

        sync = ob.GetComponent<SyncMovement>();

        if (NetworkServer.active && !sync.hasAuthority)
        {
            state.serverShouldRelayPosition = syncPosition;
            state.serverShouldRelayRotation = syncRotation;
            state.serverShouldRelayScale = syncScale;
            state.serverShouldRelayVelocity = syncVelocity;
            state.serverShouldRelayAngularVelocity = syncAngularVelocity;
        }

        if (!sync)
        {
            Debug.LogError("Could not find target for network state message.");
            return;
        }

        if (syncPosition)
        {
                if (sync.isSyncingXPosition)
                {
                    state.position.x = reader.ReadSingle();
                }
                if (sync.isSyncingYPosition)
                {
                    state.position.y = reader.ReadSingle();
                }
                if (sync.isSyncingZPosition)
                {
                    state.position.z = reader.ReadSingle();
                }
        }
        else
        {
            if (sync.stateCount > 0)
            {
                state.position = sync.stateBuffer[0].position;
            }
            else
            {
                state.position = sync.getPosition();
            }
        }
        if (syncRotation)
        {
            Vector3 rot = new Vector3();
                if (sync.isSyncingXRotation)
                {
                    rot.x = reader.ReadSingle();
                }
                if (sync.isSyncingYRotation)
                {
                    rot.y = reader.ReadSingle();
                }
                if (sync.isSyncingZRotation)
                {
                    rot.z = reader.ReadSingle();
                }
                state.rotation = Quaternion.Euler(rot);
        }
        else
        {
            if (sync.stateCount > 0)
            {
                state.rotation = sync.stateBuffer[0].rotation;
            }
            else
            {
                state.rotation = sync.getRotation();
            }
        }
        if (syncScale)
        {
                if (sync.isSyncingXScale)
                {
                    state.scale.x = reader.ReadSingle();
                }
                if (sync.isSyncingYScale)
                {
                    state.scale.y = reader.ReadSingle();
                }
                if (sync.isSyncingZScale)
                {
                    state.scale.z = reader.ReadSingle();
                }
        }
        else
        {
            if (sync.stateCount > 0)
            {
                state.scale = sync.stateBuffer[0].scale;
            }
            else
            {
                state.scale = sync.getScale();
            }
        }
        if (syncVelocity)
        {
                if (sync.isSyncingXVelocity)
                {
                    state.velocity.x = reader.ReadSingle();
                }
                if (sync.isSyncingYVelocity)
                {
                    state.velocity.y = reader.ReadSingle();
                }
                if (sync.isSyncingZVelocity)
                {
                    state.velocity.z = reader.ReadSingle();
                }
        }
        else
        {
            state.velocity = Vector3.zero;
        }
        if (syncAngularVelocity)
        {
                if (sync.isSyncingXAngularVelocity)
                {
                    state.angularVelocity.x = reader.ReadSingle();
                }
                if (sync.isSyncingYAngularVelocity)
                {
                    state.angularVelocity.y = reader.ReadSingle();
                }
                if (sync.isSyncingZAngularVelocity)
                {
                    state.angularVelocity.z = reader.ReadSingle();
                }
        }
        else
        {
            state.angularVelocity = Vector3.zero;
        }
    }



    byte encodeSyncInformation(bool sendPosition, bool sendRotation, bool sendScale, bool sendVelocity, bool sendAngularVelocity)
    {
        byte encoded = 0;

        if (sendPosition)
        {
            encoded = (byte)(encoded | positionMask);
        }
        if (sendRotation)
        {
            encoded = (byte)(encoded | rotationMask);
        }
        if (sendScale)
        {
            encoded = (byte)(encoded | scaleMask);
        }
        if (sendVelocity)
        {
            encoded = (byte)(encoded | velocityMask);
        }
        if (sendAngularVelocity)
        {
            encoded = (byte)(encoded | angularVelocityMask);
        }
        return encoded;
    }


    bool shouldSyncPosition(byte syncInformation)
    {
        if ((syncInformation & positionMask) == positionMask)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool shouldSyncRotation(byte syncInformation)
    {
        if ((syncInformation & rotationMask) == rotationMask)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool shouldSyncScale(byte syncInformation)
    {
        if ((syncInformation & scaleMask) == scaleMask)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool shouldSyncVelocity(byte syncInformation)
    {
        if ((syncInformation & velocityMask) == velocityMask)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool shouldSyncAngularVelocity(byte syncInformation)
    {
        if ((syncInformation & angularVelocityMask) == angularVelocityMask)
        {
            return true;
        }
        else
        {
            return false;
        }
    }



}
